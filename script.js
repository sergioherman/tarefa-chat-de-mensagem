const btnEnv = document.querySelector(".btnEnv")
const txtArea = document.querySelector("#txtArea")
const containerMsg = document.querySelector(".histMsg")

btnEnv.addEventListener("click", () => {
    if (txtArea.value != ''){   
        containerMsg.innerHTML += 
            `
            <div class='msgEBotoes'>
                <fieldset class='fieldMsg'>`+ txtArea.value.replaceAll('\n','</br>') +`</fieldset>
                <div class='divBtnsEditExcl'>
                    <button style='background-color: #eda563;' class='btnEdit' onclick='editar(this)'>Editar</button>
                    <button style='background-color: #f06565;' class='btnExcl' oncLick='excluir(this)'>Excluir</button>
                </div>
                <div id="elemEdit">
                </div>
            </div>

            `
        txtArea.value = ''
        //Manter a barra de rolagem em baixo
        var height = containerMsg.scrollHeight;
        containerMsg.scrollTop = height;
    }
})

function excluir(elemento){
    elemento.parentElement.parentElement.remove()
}

var editando = false
function editar(elemento){
    if (editando == false){        
        elemento.parentElement.parentElement.querySelector("#elemEdit").innerHTML = 
        `
        <button style='background-color: #008000;' class='btnExcl' oncLick='concluir(this)'>Concluir</button>
        <textarea name="" id="txtAreaEdit" cols="40" rows="4"></textarea>
        
        `
        editando = true
        var txtAreaEdit = document.querySelector("#txtAreaEdit")
        txtAreaEdit.value = elemento.parentElement.parentElement.querySelector(".fieldMsg").innerText
    }
}

function concluir(elemento){
    editando = false
    var txtEditado = elemento.parentElement.querySelector("#txtAreaEdit").value
    elemento.parentElement.parentElement.querySelector(".fieldMsg").innerText = txtEditado
    elemento.parentElement.parentElement.querySelector("#elemEdit").innerHTML = 
    `
    <div id="elemEdit">
    </div>
    `
}
